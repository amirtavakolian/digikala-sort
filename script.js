
$("a").click(function(e){
  e.preventDefault();

  let resultBox = $('.res');
  var href = $(this).attr('href');

  $.ajax({
    type: 'POST',
    url: 'proccess.php',
    data: href,
    timeout: 20000,

    success: function(response){
      resultBox.html(response);
    },
    error: function(){
      resultBox.html("Error");
    }
  });
});