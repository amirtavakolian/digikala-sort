<?php

if (isset($_POST['sts'])) {
  $select = $_POST['sts'];
} else {
  $select = '';
}
const DB = 'products';
const HOST = 'localhost';
const USER = 'root';
const PASS = '';

switch ($select) {
  case '1':
    $select = "price DESC";
    break;

  case '2':
    $select = "view DESC";
    break;

  case '3':
    $select = "price ASC";
    break;

  default:
    $select = 'id';
    break;
}

$cnt = new mysqli(HOST, USER, PASS, DB);

$qury = "SELECT * FROM categories ORDER BY $select";


$runQuery = $cnt->query($qury);
$row = $runQuery->fetch_all(MYSQLI_ASSOC);


?>

<div class="row">
  <?php foreach ($row as $key => $value) { ?>

    <div class="col-md-3">
      <div class="card" style="width: 18rem; height:2rem;">
        <img class="card-img-top" src="<?= $value['img']; ?>" style="width:286px; height:390px;" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title"><?= $value['product_name']; ?></h5>
          <h6>View: </h6>
          <p class="card-text"><?= $value['content']; ?></p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
    </div>


  <?php } ?>

</div>